resource "random_string" "lc_suffix" {
  length  = 10
  special = false
  upper   = true
  number  = true
}

resource "aws_launch_configuration" "epp-api-ecs-launch-configuration" {
  name                        = "EPP-API-Labs-ECS-${random_string.lc_suffix.result}"
  image_id                    = "${var.image_id}"
  instance_type               = "${var.instance_type}"
  enable_monitoring           = true
  iam_instance_profile        = "${data.aws_iam_role.ecsInstanceRole.name}"

  root_block_device {
    volume_type           = "standard"
    volume_size           = 22
    delete_on_termination = true
  }

  lifecycle {
    create_before_destroy = true
  }

  associate_public_ip_address = true
  security_groups             = ["${var.security_group}"]
  user_data                   = "#!/bin/bash \necho ECS_CLUSTER=${var.ecs_cluster_name} >> /etc/ecs/ecs.config;echo ECS_BACKEND_HOST= >> /etc/ecs/ecs.config;"
}
