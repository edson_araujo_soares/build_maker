resource "aws_route53_record" "labs_epp_api" {
  zone_id = "${var.hosted_zone_id}"
  name    = "${var.application_domain_name}"
  type    = "A"

  depends_on = ["aws_alb.epp-api-alb-ecs"]

  alias {
    zone_id                = "${aws_alb.epp-api-alb-ecs.zone_id}"
    name                   = "${aws_alb.epp-api-alb-ecs.dns_name}"
    evaluate_target_health = true
  }
}
