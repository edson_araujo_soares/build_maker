ecs_cluster_name="epp-api-labs"
hosted_zone_id = "Z2EMNYQ8B5WUSP"

/* Note.: You should be importing the public ssh key file only. */
aws_key_pair_name = "us-aws-public-key"
aws_key_pair_file = "~/.ssh/us-aws-public-key.pem"

region = "us-east-1"
vpc_id = "vpc-a0625fc5"
application_running_port = "80"
image_id = "ami-0254e5972ebcd132c"
security_group = "sg-0893e140"
instance_type = "t2.micro"
ecs_service_desired_capacity = 4
ecs_service_health_check_grace_period = 30
application_domain_name = "labs.eppapi.spaces.com.br"
ssl_certificate_domain_root = "eppapi.spaces.com.br"

container_cpu = "448"
container_memory = "450"
ecs_service_name = "EPP API"
ecr_image = "006375924004.dkr.ecr.us-east-1.amazonaws.com/epp-api"
