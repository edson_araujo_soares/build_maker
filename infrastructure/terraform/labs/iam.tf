/* -- ECS Service Autoscaling Role -- */
data "aws_iam_role" "ecsAutoscaleRole" {
  name = "ecsAutoscaleRole"
}

/* -- ECS Service Role -- */
data "aws_iam_role" "ecs-service-role" {
  name = "ecs-service-role"
}

/* -- EC2 Instance Role -- */
data "aws_iam_role" "ecsInstanceRole" {
  name = "ecsInstanceRole"
}
