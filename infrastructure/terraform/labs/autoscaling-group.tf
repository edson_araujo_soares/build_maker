resource "aws_autoscaling_group" "epp-api-labs-ecs-autoscaling-group" {
  name             = "EPP-API-Labs-ECS"
  max_size         = "${var.ec2_autoscale_max_instance_size}"
  min_size         = "${var.ec2_autoscale_min_instance_size}"
  desired_capacity = "${var.ec2_autoscale_desired_capacity}"

  vpc_zone_identifier   = ["${var.private_subnets}"]
  availability_zones    = ["${var.availability_zones}"]
  launch_configuration  = "${aws_launch_configuration.epp-api-ecs-launch-configuration.name}"
  health_check_type     = "ELB"
  termination_policies  = ["OldestInstance"]

  tag {
    key = "Name"
    value = "ECS-Cluster-EPP-API-Labs"
    propagate_at_launch = true
  }
}
