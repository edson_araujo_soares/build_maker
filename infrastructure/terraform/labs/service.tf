resource "aws_ecs_service" "epp-api-ecs-service" {
  name                              = "EPP-API-Labs"
  cluster                           = "${aws_ecs_cluster.EPP-API.id}"
  task_definition                   = "${aws_ecs_task_definition.epp-api-container-definition.arn}"
  iam_role                          = "${data.aws_iam_role.ecs-service-role.name}"
  desired_count                     = "${var.ecs_service_desired_capacity}"
  health_check_grace_period_seconds = "${var.ecs_service_health_check_grace_period}"

  load_balancer {
    container_port    = 80
    container_name    = "EPP-API"
    target_group_arn  = "${aws_alb_target_group.epp-api-elb.arn}"
  }

}

resource "aws_appautoscaling_target" "ecs_cluster_target" {
  max_capacity       = "${var.ecs_autoscale_max_instance_size}"
  min_capacity       = "${var.ecs_service_desired_capacity}"
  resource_id        = "service/${aws_ecs_cluster.EPP-API.name}/${aws_ecs_service.epp-api-ecs-service.name}"
  role_arn           = "${data.aws_iam_role.ecsAutoscaleRole.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_cluster_autoscaling_down_policy" {
  name                    = "ContainersScaleDown"
  policy_type             = "StepScaling"
  resource_id             = "service/${aws_ecs_cluster.EPP-API.name}/${aws_ecs_service.epp-api-ecs-service.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -2
    }
  }

  depends_on = ["aws_appautoscaling_target.ecs_cluster_target"]
}

resource "aws_cloudwatch_metric_alarm" "ecs-cluster-instances-low-cpu-utilization" {
  alarm_name          = "EPP-API-Labs-ECS-Low-CPU-Utilization"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "30"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.epp-api-labs-ecs-autoscaling-group.name}"
  }

  alarm_actions = ["${aws_appautoscaling_policy.ecs_cluster_autoscaling_down_policy.arn}"]
}

resource "aws_appautoscaling_policy" "ecs_cluster_autoscaling_up_policy" {
  name                    = "ContainersScaleUp"
  policy_type             = "StepScaling"
  resource_id             = "service/${aws_ecs_cluster.EPP-API.name}/${aws_ecs_service.epp-api-ecs-service.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment = 2
    }
  }

  depends_on = ["aws_appautoscaling_target.ecs_cluster_target"]
}

resource "aws_cloudwatch_metric_alarm" "ecs-cluster-instances-high-cpu-utilization" {
  alarm_name          = "EPP-API-Labs-ECS-High-CPU-Utilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "60"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.epp-api-labs-ecs-autoscaling-group.name}"
  }

  alarm_actions = ["${aws_appautoscaling_policy.ecs_cluster_autoscaling_up_policy.arn}"]
}
