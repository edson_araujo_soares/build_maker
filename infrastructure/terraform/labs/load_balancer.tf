resource "aws_alb_target_group" "epp-api-elb" {
  name        = "EPP-API-Labs-Cluster-ALB"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = "${var.vpc_id}"
  target_type = "instance"

  lifecycle {
    create_before_destroy = true
  }

  health_check {
    healthy_threshold   = "5"
    unhealthy_threshold = "2"
    interval            = "30"
    matcher             = "200"
    path                = "/"
    protocol            = "HTTP"
    timeout             = "5"
  }

  depends_on = [
    "aws_alb.epp-api-alb-ecs"
  ]

  tags {
    Name = "epp-api-labs-ecs-cluster"
  }
}

resource "aws_alb" "epp-api-alb-ecs" {
  name            = "epp-api-labs-alb-ecs"
  subnets         = ["${var.public_subnets}"]
  security_groups = ["${var.security_group}"]
}

resource "aws_alb_listener" "app-api-listener" {
  load_balancer_arn = "${aws_alb.epp-api-alb-ecs.id}"
  port			    = 443
  protocol		    = "HTTPS"
  certificate_arn   = "${data.aws_acm_certificate.eppapi_ssl_certificate.arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.epp-api-elb.id}"
    type             = "forward"
  }
}
