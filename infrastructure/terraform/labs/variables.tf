variable "region" {
  description = "The AWS region to create resources in."
}

variable "aws_key_pair_name" {
  description = "The AWS key Pair Name."
}

variable "aws_key_pair_file" {
  description = "The AWS key Pair File."
}

variable "image_id" {
  description = ""
}

variable "vpc_id" {
  description = "The Labs VPC."
}

variable "hosted_zone_id" {
  description = "R53 Domain Hosted Zone"
}

variable "availability_zones" {
  default = [
    "us-east-1e",
    "us-east-1c",
    "us-east-1b",
    "us-east-1a"
  ]
  type = "list"
}

variable "private_subnets" {
  default = [
    "subnet-23e16b08",
    "subnet-c06950fa",
    "subnet-6d096934",
    "subnet-3ed89449"
  ]
  type = "list"
}

variable "public_subnets" {
  default = [
    "subnet-22eef355",
    "subnet-f3c6f1aa",
    "subnet-62569f5f",
    "subnet-a2e16b89"
  ]
  type = "list"
}

variable "ecs_service_name" {
  description = "The name of the containers on ECS."
}

variable "ecr_image" {
  description = ""
}

variable "ecr_image_version" {
  description = ""
}

variable "container_cpu" {
  description = ""
}

variable "container_memory" {
  description = ""
}

variable "ecs_cluster_name" {
  description = "The name of the Amazon ECS cluster."
}

variable "ec2_autoscale_min_instance_size" {
  default = "2"
  description = "Minimum autoscale (number of EC2)"
}

variable "ec2_autoscale_max_instance_size" {
  default = "25"
  description = "Maximum autoscale (number of EC2)"
}

variable "ec2_autoscale_desired_capacity" {
  default = "2"
  description = "Desired autoscale (number of EC2)"
}

variable "ecs_autoscale_max_instance_size" {
  default = "50"
  description = "Maximum autoscale for the cluster (number of Docker containers)"
}

variable "instance_type" {
  description = ""
}

variable "security_group" {
  description = ""
}

variable "application_domain_name" {
  description = ""
}

variable "ssl_certificate_domain_root" {
  description = ""
}

variable "application_running_port" {
  description = "Port which the application will be running in"
}

variable "ecs_service_desired_capacity" {
  description = "The number of instantiations of the specified task definition to place and keep running on your cluster."
}

variable "ecs_service_health_check_grace_period" {
  description = "This instructs the service scheduler to ignore ELB health checks for a pre-defined time period after a task has been instantiated."
}
