data "template_file" "epp-api-container-template" {
  template = "${file("container-definitions.json")}"

  vars {
    container_name = "${var.ecs_service_name}"
    image          = "${var.ecr_image}"
    version        = "${var.ecr_image_version}"
    cpu            = "${var.container_cpu}"
    memory         = "${var.container_memory}"
  }
}

resource "aws_ecs_task_definition" "epp-api-container-definition" {
  lifecycle {
    create_before_destroy = true
  }

  family = "EPP-API-Labs"
  container_definitions = "${data.template_file.epp-api-container-template.rendered}"
}
