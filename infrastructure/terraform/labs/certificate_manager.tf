data "aws_acm_certificate" "eppapi_ssl_certificate" {
  domain = "${var.ssl_certificate_domain_root}"
}
