/**

Build Pipeline in steps:

 - CSM Checkout
 - Build Testing Image
 - Testing
    - Unit Testing
    - API Testing
    - Integration Testing ( Registro.Br EPP production )
 - QA Analysis
 - Build AWS Image
 - Push AWS Image ( ECS Repository )
 - Deploy to Labs
 - Deploy to Staging
 - Promote to Production
     - Rollback
     - CleanUp Inactive Release

*/

pipeline {
    agent any

    environment {
        image_name                    = 'epp-api'
        dockerfile                    = 'AWS.Dockerfile'
        dockerfile_test               = 'Test.Dockerfile'
        docker_image                  = ''
        infrastructure_root_directory = 'infrastructure/terraform'
    }

    options {
        skipDefaultCheckout()
        skipStagesAfterUnstable()
    }

    stages {

        stage ('SCM Checkout') {
            steps {

                script {
                    buildAWSInfrastructure("${currentBuild.number}", 'labs', 'staging')
                }

                checkout([
                    $class: 'GitSCM',
                    branches: [[name: '*/master']],
                    extensions: [[$class: 'CleanBeforeCheckout']],
                    userRemoteConfigs: [[
                        credentialsId: 'e63d4925-578e-4950-b39e-74d9423b6003',
                        url: 'https://edson_araujo_soares@bitbucket.org/edson_araujo_soares/build_maker.git'
                    ]]
                ])

            }
        }

        stage ('Build Testing Image') {
            when {
                anyOf {
                    changeset "**/*.h";
                    changeset "**/*.sh"
                    changeset "**/*.cpp"
                    changeset "${dockerfile}"
                    changeset "**/CMakeLists.txt"
                }
            }
            steps {
                script {
                    docker.build("${image_name}-test:latest",  "-f ${dockerfile_test} .")
                }
            }
        }

        stage ('Testing') {
            steps {

                parallel (
                    "Integration Testing" : {
                        echo 'Running API integration tests'
                    },
                    "API Testing" : {
                        echo 'Running API tests'
                    },
                    "Unit Testing" : {
                        script {
                            /*
                             * Run some tests which require specific dependencies installed, and assume that
                             * the image has been installed with tests available.
                             */
                            docker.image("${image_name}-test:latest").inside('--user root') {
                                sh 'cd /tmp/epp_api/bin && ./tests --gtest_filter=* --gtest_color=yes'
                            }
                        }
                    }
                )

            }   // Steps
        }       // Stage

        stage ('QA') {
            environment {
                SONAR_SCANNER_OPTS="-Xmx512m"
            }
            steps {
                script {

                    /*
                     * Run scripts for generating SonarQube QA reports:
                     *  - CppCheck      (Source code static analysis checks)
                     *  - Valgrind      (Debugging memory leaks & errors)
                     *  - Gcov & Gcovr  (Unit test coverage report )
                     *  - Google Tests  (Unit test execution report)
                     */
                    docker.image("${image_name}-test:latest").inside('--user root') {

                        sh 'OUTPUT_DIR=$(pwd) && cd /tmp/epp_api && ./bin/tests --gtest_filter=* --gtest_color=yes \
                                        --gtest_output="xml:$OUTPUT_DIR/xunit_report.xml"'

                        sh 'OUTPUT_DIR=$(pwd) && cd /tmp/epp_api && gcovr --keep -r . --object-directory=. \
                                        && gcovr -r . -x --object-directory=. > $OUTPUT_DIR/gcovr-report.xml'

                        sh 'cppcheck --xml --verbose --xml-version=2 --enable=all --suppress=missingIncludeSystem \
                                        --language=c++ \
                                        /tmp/epp_api/src \
                                        /tmp/epp_api/include \
                                        /tmp/epp_api/test 2> cppcheck-report.xml'

                        sh 'valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose \
                                        --xml=yes --xml-file=valgrind-report.xml \
                                        /tmp/epp_api/bin/tests'

                    }

                }   // Script

                withSonarQubeEnv('SonarQube') {
                    sh 'sonar-scanner -X'
                }

            }       // Steps
        }           // Stage

        stage ('Build AWS Image') {
            when {
                changeset "**/*.h";
                changeset "**/*.sh"
                changeset "**/*.cpp"
                changeset "${dockerfile}"
                changeset "**/CMakeLists.txt"
            }

            steps {
                script {
                    docker_image = docker.build("${image_name}:${currentBuild.number}",  "-f ${dockerfile} .")
                }
            }
        }

        stage ('Push AWS Image') {
            when {
                changeset "**/*.h";
                changeset "**/*.sh"
                changeset "**/*.cpp"
                changeset "${dockerfile}"
                changeset "**/CMakeLists.txt"
            }
            steps {
                script {

                    docker.withRegistry('https://006375924004.dkr.ecr.us-east-1.amazonaws.com/epp-api', 'ecr:us-east-1:aws-general-credentials') {
                        docker_image.push()
                        // It pushes this image also as the latest version.
                        docker_image.push('latest')
                    }

                }
            }
        }

        stage ('Deploy to Labs') {
            steps {
                script {

                    current_env = 'labs'
                    def create_labs_infrastructure
                    try {

                        echo 'Wait 30 seconds for the answer.'
                        timeout(time: 30, unit: 'SECONDS') {
                            create_labs_infrastructure = input id: 'create_labs_infrastructure',
                                message: 'Create EPP API Labs infrastructure?',
                                ok: 'Yes, do it.',
                                parameters: [
                                    [
                                        $class: 'BooleanParameterDefinition',
                                        defaultValue: true,
                                        successfulOnly: true,
                                        description: 'Please confirm you agree with this.',
                                        name: 'I agree with it.'
                                    ]
                                ]
                        }

                    } catch (error) {

                        def user = error.getCauses()[0].getUser()
                        if( 'SYSTEM' == user.toString() ) {
                            // The constant SYSTEM means that the build was cancelled by timeout.
                            echo 'No user interaction. Build timeout.'
                            currentBuild.result = 'ABORTED'
                            throw error
                        }

                        // It means that the build was cancelled by a user, but It will continue.
                        currentBuild.result = 'SUCCESS'
                        echo "Stage aborted by [${user}]"

                    }

                    if ( create_labs_infrastructure == true ) {

                        sh """
                            #!/bin/bash

                            echo 'Building Labs Infrastructure with Terraform ...'
                            cd ${infrastructure_root_directory}/${current_env} && terraform init
                            terraform plan  -var-file=terraform.tfvars -var ecr_image_version=latest
                            terraform apply -var-file=terraform.tfvars -var ecr_image_version=latest -auto-approve

                        """

                        // It puts the build to sleep for some time.
                        sleep(time:5, unit:"MINUTES")

                        def destroy_labs_infrastructure
                        try {

                            echo 'Wait 8 hours for the answer.'
                            timeout(time: 8, unit: 'HOURS') {
                                destroy_labs_infrastructure = input id: 'destroy_labs_infrastructure',
                                    message: 'Destroy EPP API Labs infrastructure before continue?',
                                    ok: 'Yes, do it.',
                                    parameters: [
                                        [
                                            $class: 'BooleanParameterDefinition',
                                            defaultValue: true,
                                            successfulOnly: true,
                                            description: 'Please confirm you agree with this.',
                                            name: 'I agree with it.'
                                        ]
                                    ]
                            }

                        } catch (error) {
                            currentBuild.result = 'SUCCESS'
                        }

                        // Destroy infrastructure it was required for the user.
                        if ( destroy_labs_infrastructure == true ) {
                            echo 'Destroying Labs Infrastructure with Terraform ...'
                            // sh "cd ${infrastructure_root_directory}/${current_env} && terraform destroy -auto-approve"
                        }

                    }

                }   // Script
            }       // Steps
        }           // Stage

        stage ('Deploy to Staging') {
            steps {
                script {

                    def current_env = 'production'
                    def create_production_infrastructure
                    try {

                        echo 'Wait 5 minutes for the answer.'
                        timeout(time: 5, unit: 'MINUTES') {

                            create_production_infrastructure = input id: 'create_production_infrastructure',
                                message: 'Confirm creation of EPP API Staging infrastructure?',
                                ok: 'Yes, deploy it.',
                                parameters: [
                                    [
                                        $class: 'BooleanParameterDefinition',
                                        defaultValue: true,
                                        successfulOnly: true,
                                        description: 'Please confirm you agree with this.',
                                        name: 'I agree with it.'
                                    ]
                                ]

                        }

                    } catch (error) {

                        def user = error.getCauses()[0].getUser()
                        if( 'SYSTEM' == user.toString() ) {
                            // The constant SYSTEM means that the build was cancelled by timeout.
                            echo 'No user interaction. Build timeout.'
                            currentBuild.result = 'ABORTED'
                            throw error
                        }
                        currentBuild.result = 'UNSTABLE'

                    }

                    if ( create_production_infrastructure == true ) {

                        echo 'Building Production Infrastructure with Terraform ...'
                        // sh "cd ${infrastructure_root_directory}/${current_env} && terraform init "
                        // sh "cd ${infrastructure_root_directory}/${current_env} && terraform plan -var-file=terraform.tfvars \
                        //                                                                            -var ecr_image_version=1.9"

                        // sh "cd ${infrastructure_root_directory}/${current_env} && terraform apply -var-file=terraform.tfvars \
                        //                                                                             -var ecr_image_version=1.9
                        //                                                                             -auto-approve"

                    }

                }   // Script
            }       // Steps
        }           // Stage

        stage ('Release in Production') {
            steps {
                script {

                    def release_in_production
                    try {

                        echo 'Wait 5 minutes for the answer.'
                        timeout(time: 5, unit: 'MINUTES') {

                            release_in_production = input id: 'release_in_production',
                                message: 'Confirm promotion of EPP API current release to production?',
                                ok: 'Yes, release it.',
                                parameters: [
                                    [
                                        $class: 'BooleanParameterDefinition',
                                        defaultValue: true,
                                        successfulOnly: true,
                                        description: 'Please confirm you agree with this.',
                                        name: 'I agree with it.'
                                    ]
                                ]

                        }

                    } catch (error) {

                        def user = error.getCauses()[0].getUser()
                        if( 'SYSTEM' == user.toString() ) {

                        }

                    }

                    // It publishes the current new version of the application in production.
                    if ( release_in_production == true ) {
                        echo 'Releasing current version of the application in production ...'
                        // sh ""

                        def option

                        echo 'Wait 8 hours for the answer.'
                        timeout(time: 8, unit: 'HOURS') {
                            option = input message: 'After releasing a new version of the application in production you need to choose what is the next action to perform.',
                                ok: 'Apply it!',
                                    parameters: [
                                        choice(
                                            name: 'after_production_release',
                                            choices: 'Rollback\nClean Up Inactive Version',
                                            description: 'What should be done next:'
                                        )
                                    ]
                        }

                        if ( option.equals("Rollback") ) {
                            echo "You chose Rollback."
                        } else {
                            echo "You chose CleanUp Inactive version."
                        }

                    } else { // Release permission denied by user.

                        // Destroy current release staging infrastructure.
                        echo "Setting the Build to UNSTABLE"

                        currentBuild.result = 'UNSTABLE'

                    }

                }

            }   // Steps
        }       // Stage

    }   // Stages
}       // Pipeline

//
def buildAWSInfrastructure(build_number, environment_name, image_version = 'latest', terraform_workspace) {

    sh """
        #!/bin/bash

        terraform workspace new ${terraform_workspace}

    """

    echo "Version:       [${build_number}]"
    echo "Environment:   [${image_version}]"
    echo "Image Version: [${environment_name}]"

}

//
def doPaste(name) {

}

//
def doSave(name) {

}

//
def doDelete(name) {

}
