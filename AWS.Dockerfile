FROM impactantehub/eppapi-basic-environment:1.9

ENV DEBIAN_FRONTEND noninteractive

ENV CMAKE_DIRECTORY_NAME cmake-3.7.2
ENV CMAKE_FILE_NAME cmake-3.7.2.tar.gz
ENV CMAKE_DOWNLOAD_URL https://cmake.org/files/v3.7/cmake-3.7.2.tar.gz

ENV NIC_EPPLIB_REPOSITORY_URL https://bitbucket.org/_impactante/epp_lib.git

ENV XERCES_DIRECTORY_NAME xerces-c-3.2.2
ENV XERCES_FILE_NAME xerces-c-3.2.2.tar.gz
ENV XERCES_DOWNLOAD_URL http://ftp.unicamp.br/pub/apache//xerces/c/3/sources/xerces-c-3.2.2.tar.gz

ENV CMAKE_SHA256 dc1246c4e6d168ea4d6e042cfba577c1acd65feea27e56f5ff37df920c30cae0
ENV XERCES_SHA256 dd6191f8aa256d3b4686b64b0544eea2b450d98b4254996ffdfe630e0c610413

# Install application dependencies
RUN apt-get update -qq \
    && dpkg --add-architecture i386 \
    && apt-get update \
	&& apt-get install -yq --no-install-recommends \
		git \
		openssl \
		libgtest-dev \
		libssl-dev:i386 \
		curl

# Download and Install CMake 3.7.2
RUN curl -fsSL "${CMAKE_DOWNLOAD_URL}" -o /tmp/cmake.tar.gz \
    && echo "$CMAKE_SHA256  /tmp/cmake.tar.gz" | sha256sum -c - \
    && tar --directory /tmp -xzf /tmp/cmake.tar.gz \
    && cd /tmp/$CMAKE_DIRECTORY_NAME && ./configure \
    && make && make install \
    && rm -rf /tmp/$CMAKE_FILE_NAME /tmp/cmake.tar.gz

# It makes available Google Test
RUN cd /usr/src/gtest \
    && cmake . \
    && make && cp *.a /usr/lib

# Install Apache Xerces
RUN curl -fsSL "${XERCES_DOWNLOAD_URL}" -o /tmp/xerces.tar.gz \
    && echo "$XERCES_SHA256  /tmp/xerces.tar.gz" | sha256sum -c - \
    && tar --directory /tmp -xzf /tmp/xerces.tar.gz \
    && cd /tmp/$XERCES_DIRECTORY_NAME && ./configure --disable-static CFLAGS=-O3 CXXFLAGS=-O3 \
    && make && make install \
    && rm -rf /tmp/$XERCES_FILE_NAME /tmp/xerces.tar.gz

# Install NIC.Br EPP Library
RUN git clone "${NIC_EPPLIB_REPOSITORY_URL}" /tmp/epplib \
    && cd /tmp/epplib && cmake . \
    && make && make install \
    && rm -rf /tmp/epplib

# The variable expansion technique used in this variable definition allows passes arguments
# from the command line and persist them in the final image by leveraging the ENV instruction.
ARG EPP_SERVICE_ENVIRONMENT
ENV EPP_SERVICE_ENVIRONMENT=${EPP_SERVICE_ENVIRONMENT:-homologation}

# It sets the build-time parameters default values
ARG CMAKE_BUILD_TYPE=Release

# Install EPP API
COPY . /tmp/epp_api
# RUN cd /tmp/epp_api \
#    && cmake . \
#        -DENABLE_TESTS=ON \
#        -DENABLE_CUSTOM_CLI=ON \
#        -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} \
#    && make \
#    && cd /tmp/epp_api/bin && ./tests --gtest_filter=* --gtest_color=yes \
#    && cd /tmp/epp_api && make install \
#    && rm -rf /tmp/epp_api

# Add the Apache Connector file configuration file to the image
COPY docs/settings/poco.load /etc/apache2/mods-available/poco.load

# It removes packages that were necessary only over the build-time.
RUN apt-get purge -qq \
        git \
        libgtest-dev \
        curl

COPY apache-setup-for-dockerfile.sh /tmp/apache-setup-for-dockerfile.sh

RUN chmod +x /tmp/apache-setup-for-dockerfile.sh \
    && /usr/sbin/apache2ctl stop \
    && /tmp/apache-setup-for-dockerfile.sh $EPP_SERVICE_ENVIRONMENT \
    && rm /tmp/apache-setup-for-dockerfile.sh

CMD ["/usr/sbin/apache2ctl", "-DFOREGROUND"]
