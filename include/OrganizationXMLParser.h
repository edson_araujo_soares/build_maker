#ifndef Infrastructure_Parser_OrganizationXMLParser_INCLUDED
#define Infrastructure_Parser_OrganizationXMLParser_INCLUDED

namespace Infrastructure {
namespace Parser {


    class OrganizationXMLParser
    {
    public:
        OrganizationXMLParser() = default;

        bool objectAvailable();

    private:
        int unitialized;
    };


} }

#endif
