cmake_minimum_required(VERSION 3.7)

set(CMAKE_C_COMPILER /usr/bin/gcc)
set(CMAKE_CXX_COMPILER /usr/bin/g++)

# It sets default to Release build type
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
    message(STATUS "CMake Build Type: ${CMAKE_BUILD_TYPE}")
else()
    message(STATUS "CMake Build Type: ${CMAKE_BUILD_TYPE}")
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wextra -Wall -Wpedantic")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} \
         -fprofile-arcs -ftest-coverage \
        -fPIC -g -O0 -rdynamic -Wshadow -Wunused-parameter -Wunused-variable -Wdelete-non-virtual-dtor -Wfatal-errors \
        -Woverloaded-virtual -Wunreachable-code -Wno-unused -Wundef -Wl,--no-undefined -Wl,--no-allow-shlib-undefined")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3 -w -Wl,--discard-all")

#### handling with names ####################################

# It removes the prefix "lib" from the .so file of the application
SET(CMAKE_SHARED_LIBRARY_PREFIX "")

# application's CLI executable name
set(EPP_API_CLI_EXECUTABLE_NAME epp)

# application's binary name
set(EPP_API_BINARY_NAME epp_restful_api)

# application's root directory name
set(EPP_API_HOSTING_DIRECTORY_NAME ${EPP_API_BINARY_NAME})

#### handling with paths ####################################

# where the bin file will be located
set(EPP_API_BINARY_DIRECTORY ${CMAKE_INSTALL_PREFIX}/bin)

# where the configuration's file will be located
set(EPP_API_CONFIG_DIRECTORY ${CMAKE_INSTALL_PREFIX}/etc/${EPP_API_HOSTING_DIRECTORY_NAME})

# where the modules of the application will be located
set(EPP_API_LIBRARY_DIRECTORY ${CMAKE_INSTALL_PREFIX}/lib)

#### handling basic setup ####################################

set(PROJECT_INCLUDE_DIR ${CMAKE_HOME_DIRECTORY}/include)
set(PROJECT_SOURCE_DIR ${CMAKE_HOME_DIRECTORY}/src)

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY}/lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY}/bin)

file(GLOB SOURCE_FILES "${PROJECT_SOURCE_DIR}/*.cpp")

include_directories("${PROJECT_INCLUDE_DIR}")
include_directories("${PROJECT_SOURCE_DIR}")

add_library(${EPP_API_BINARY_NAME} SHARED ${SOURCE_FILES})

#### handling options ########################################

option(ENABLE_TESTS
    "Set to OFF|ON (default: OFF) to control build of EPP API tests" OFF)

option(ENABLE_CUSTOM_CLI
    "Set to OFF|ON (default: ON) to control build of EPP API Custom CLI" ON)

if (ENABLE_TESTS)
    enable_testing()
    add_subdirectory(test)

    message(STATUS "Building with tests")
elseif ()
    message(STATUS "Building without tests")
endif ()
